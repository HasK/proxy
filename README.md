# proxy.py #

proxy.py - simple **TCP & UDP** proxy tool.
It can start listening server on specified host:port and will forward all accepted connections to other specified host:port.
In UDP redirection mode sockets will live until they communicating plus some timeout (--timeout option).

Supported coloring of incoming/outgoing data, formatting all passing data as HEX/REPR/TEXT/NONE and logging to a file.

## License ##

New BSD License. You can use it (if you want) everywhere, "AS IS", etc...

## Features ##

run `./proxy.py --help` :


    usage: proxy.py [-h] [--no-colors] [--out {hex,repr,text,none}]
                    [--log FILENAME] [--udp] [--timeout TIMEOUT]
                    acc_host acc_port to_host to_port

    Proxy all incoming TCP/UDP connections to other target TCP/UDP server

    positional arguments:
      acc_host              host to accept incoming connections
      acc_port              port to accept incoming connections
      to_host               target host to forward incoming connections
      to_port               target port to forward incoming connections

    optional arguments:
      -h, --help            show this help message and exit
      --no-colors           switch off coloring of terminal
      --out {hex,repr,text,none}
                            choose data output format
      --log FILENAME        specify log file name
      --udp                 use UDP redirection instead of TCP
      --timeout TIMEOUT     UDP sessions timeout, in seconds

    Written by xhaskx@gmail.com, https://bitbucket.org/HasK/proxy




## Example ##

run `./proxy.py "" 10001 localhost 10000 --format repr` :

    -2014/06/03 23:20:33.182469 [SYS] Starting TCP proxy :10001 --> localhost:10000...
    -2014/06/03 23:20:38.861202 [SYS] Accepted new connection from 127.0.0.1:44386
    -2014/06/03 23:20:38.861598 [SYS] Created pipe 127.0.0.1:44386 --> localhost:10000
    -2014/06/03 23:20:53.548324 [<--] 127.0.0.1:44386: 'test msg from client\n'
    -2014/06/03 23:21:00.618613 [-->] localhost:10000: 'test answer from server\n'
    ^C
    -2014/06/03 23:21:21.648750 [SYS] Closing 127.0.0.1:44386 --> localhost:10000
    -2014/06/03 23:21:21.648915 [SYS] Proxy stopped :10001 --> localhost:10000

Of course, you need some listening server on localhost:10000 (I used netcat).

## Dependencies ##

There is no dependencies, working with standard python 2.7 included batteries.
Using asyncore to handle network events.

## My real purposes ##

I just wanted to play with asyncore, sockets, logging and argparse python modules, nothing more.
Leaving it as example of using it.