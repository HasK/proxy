#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import time
import socket
import logging
import asyncore
import argparse
import platform
import traceback
from datetime import datetime


# global options
IS_WINDOWS = (platform.system() == "Windows")
USE_COLORS = not IS_WINDOWS
OUTPUT = "hex"
UDP_TIMEOUT = 300


class COL:
	"terminal colors"
	GREEN = "\033[1;32m"
	GRAY = "\033[0;37m"
	WHITE = "\033[1;37m"
	END = "\033[0m"


def format_data(data):
	"format data function"
	if OUTPUT == "hex":
		return data.encode("hex").upper()
	elif OUTPUT == "repr":
		return repr(data)
	else:
		return data

def log(msg, kind=None):
	"log function"
	if not kind:
		kind = "***"
	tm = tm = datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f")
	res = "-%s [%s] %s" % (tm, kind, msg)
	if USE_COLORS:
		if kind == "SYS":
			print COL.GREEN + res + COL.END
		elif kind == "<--":
			print COL.GRAY + res + COL.END
		elif kind == "-->":
			print COL.WHITE + res + COL.END
		else:
			print res
	else:
		print res
	sys.stdout.flush()
	logging.getLogger("proxy").info(res)

def log_sys(msg):
	"log system messages"
	log(msg, kind="SYS")

def log_socket_error(msg, exc):
	"log any socket error with its description"
	msg = "%s: socket error %d: %s" % (msg, exc.errno, os.strerror(exc.errno))
	log(msg, kind="SYS")

class TcpPipe(object):
	"""Represent network pipe - two connected sockets"""
	def __init__(self, accepter, sock, addr):
		self.accepter = accepter
		# create 'from' end of pipe
		self.end_from = TcpPipeEnd(self, sock, addr, "<--")
		
		# open connection to 'to' end of pipe
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		addr = (self.accepter.to_host, self.accepter.to_port)
		sock.connect(addr)
		self.end_to = TcpPipeEnd(self, sock, addr, "-->")
		self.end_to.other, self.end_from.other = self.end_from, self.end_to
	
	def close(self):
		self.end_from.close()
		self.end_to.close()
	
	def on_close(self):
		if self in self.accepter.pipes:
			self.accepter.pipes.remove(self)
	
	def __str__(self):
		return "%s --> %s" % (self.end_from, self.end_to)


class TcpPipeEnd(asyncore.dispatcher_with_send):
	"""Represent one end of network pipe"""
	def __init__(self, pipe, sock, addr, role):
		asyncore.dispatcher_with_send.__init__(self, sock)
		self.pipe = pipe
		self.addr = addr
		self.role = role
		self.other = None
	
	def handle_read(self):
		data = self.recv(8192)
		if data:
			if OUTPUT != "none":
				log("%s: %s" % (self, format_data(data)), kind=self.role)
			self.other.send(data)
	
	def handle_close(self):
		log("Closing pipe %s by %s" % (self.pipe, self), kind=self.role)
		self.close()
		self.other.close()
		self.pipe.on_close()
	
	def handle_error(self):
		log("TcpPipeEnd exception: " + traceback.format_exc(), kind="EXCEPTION")
	
	def __str__(self):
		return "%s:%d" % self.addr


class UdpSession(asyncore.dispatcher):
	"""UDP session"""
	
	def __init__(self, accepter, addr_from, addr_to):
		asyncore.dispatcher.__init__(self)
		self.create_socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.addr_from = addr_from
		self.addr_to = addr_to
		self.accepter = accepter
		self.last_comm_time = time.time()
		log_sys("Starting UDP session %s" % self)
	
	def handle_read(self):
		try:
			pair = self.recvfrom(8192)
		except socket.error, exc:
			log_socket_error("recvfrom() in UdpSession:", exc)
		else:
			if pair is not None:
				data, addr = pair
				if OUTPUT != "none":
					log("%s:%d --> %s:%d: %s" % (self.addr_to[0], self.addr_to[1], self.addr_from[0], self.addr_from[1], format_data(data)), kind="-->")
				self.accepter.sendto(data, self.addr_from)
				self.last_comm_time = time.time()
	
	def handle_error(self):
		log("UdpSession exception: " + traceback.format_exc(), kind="EXCEPTION")
	
	def writable(self):
		# check timeout
		if time.time() - self.last_comm_time > UDP_TIMEOUT:
			self.accepter.close_udp_session(self.addr_from)
		return False
	
	def send_data(self, data):
		if OUTPUT != "none":
			log("%s:%d: %s" % (self.addr_from[0], self.addr_from[1], format_data(data)), kind="<--")
		self.sendto(data, self.addr_to)
		self.last_comm_time = time.time()
	
	def __str__(self):
		return "%s:%d --> %s:%d" % (self.addr_from[0], self.addr_from[1], self.addr_to[0], self.addr_to[1])


class AcceptingServer(asyncore.dispatcher):
	"""Incoming connections accepting server"""
	
	def __init__(self, acc_host, acc_port, to_host, to_port, is_udp):
		asyncore.dispatcher.__init__(self)
		self.acc_host = acc_host
		self.acc_port = acc_port
		self.to_host = to_host
		self.to_port = to_port
		self.is_udp = is_udp
		
		self.pipes = []
		self.udp_sessions = {}
		
		if self.is_udp:
			self.create_socket(socket.AF_INET, socket.SOCK_DGRAM)
			self.bind((self.acc_host, self.acc_port))
			log_sys("Starting UDP proxy %s:%d --> %s:%d..." % (acc_host, acc_port, to_host, to_port))
		else:
			self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
			self.set_reuse_addr()
			self.bind((self.acc_host, self.acc_port))
			self.listen(5)
			log_sys("Starting TCP proxy %s:%d --> %s:%d..." % (acc_host, acc_port, to_host, to_port))
	
	def handle_read(self):
		"recv by UDP"
		try:
			pair = self.recvfrom(8192)
		except socket.error, exc:
			log_socket_error("recvfrom() in AcceptingServer:", exc)
		else:
			if pair is not None:
				data, addr = pair
				session = self.udp_sessions.get(addr)
				if not session:
					session = UdpSession(self, addr, (self.to_host, self.to_port))
					self.udp_sessions[addr] = session
				session.send_data(data)
	
	def close_udp_session(self, addr_from):
		session = self.udp_sessions.get(addr_from)
		if session:
			log_sys("Closing UDP session %s by timeout" % session)
			del self.udp_sessions[addr_from]
			session.close()
			del session
	
	def writable(self):
		return False
	
	def handle_accept(self):
		pair = self.accept()
		if pair is not None:
			sock, addr = pair
			log_sys("Accepted new connection from %s:%d" % addr)
			self.forward_conn(sock, addr)
	
	def handle_error(self):
		log("AcceptingServer exception: " + traceback.format_exc(), kind="EXCEPTION")
	
	def forward_conn(self, sock, addr):
		pipe = TcpPipe(self, sock, addr)
		log_sys("Created pipe %s" % pipe)
		self.pipes.append(pipe)
	
	def stop_proxy(self):
		# close TCP pipes
		for pipe in self.pipes:
			log_sys("Closing TDP pipe %s" % pipe)
			pipe.close()
		
		# close UDP redirs
		for addr in self.udp_sessions:
			session = self.udp_sessions[addr]
			log_sys("Closing UDP session %s" % session)
			session.close()
		
		log_sys("Proxy stopped %s:%d --> %s:%d" % (self.acc_host, self.acc_port, self.to_host, self.to_port))


if __name__ == "__main__":
	# parse arguments
	aparser = argparse.ArgumentParser(
		description="Proxy all incoming TCP/UDP connections to other target TCP/UDP server",
		epilog="Written by xhaskx@gmail.com, https://bitbucket.org/HasK/proxy"
	)
	
	# port argument validation function
	def port(val):
		val = int(val)
		if val <= 0 or val > 0xFFFF:
			raise argparse.ArgumentTypeError("invalid port value: '%d'" % val)
		return val
	
	def uint(val):
		val = int(val)
		if val < 0:
			raise argparse.ArgumentTypeError("invalid uint value: '%d'" % val)
		return val
	
	# define arguments
	aparser.add_argument("acc_host", help="host to accept incoming connections")
	aparser.add_argument("acc_port", help="port to accept incoming connections", type=port)
	aparser.add_argument("to_host", help="target host to forward incoming connections")
	aparser.add_argument("to_port", help="target port to forward incoming connections", type=port)
	aparser.add_argument("--no-colors", help="switch off coloring of terminal", action="store_true")
	aparser.add_argument("--out", help="choose data output format", choices=["hex", "repr", "text", "none"], default="hex")
	aparser.add_argument("--log", help="specify log file name", metavar="FILENAME")
	aparser.add_argument("--udp", help="use UDP redirection instead of TCP", action="store_true")
	aparser.add_argument("--timeout", help="UDP sessions timeout, in seconds", type=uint, default=UDP_TIMEOUT)
	
	# parse arguments
	args = aparser.parse_args()
	
	# use arguments to set options
	if args.no_colors:
		USE_COLORS = False
	
	OUTPUT = args.out
	
	if args.log:
		# setup logging to file
		logger = logging.getLogger("proxy")
		handler = logging.FileHandler(args.log)
		handler.setFormatter(logging.Formatter("%(message)s"))
		logger.addHandler(handler) 
		logger.setLevel(logging.INFO)
	
	UDP_TIMEOUT = args.timeout
	
	# start accepting server
	accepter = AcceptingServer(args.acc_host, args.acc_port, args.to_host, args.to_port, args.udp)
	try:
		# start asyncore loop
		asyncore.loop(30, not IS_WINDOWS) # timeout=30, use_epool=True (not on Windows)
	except KeyboardInterrupt:
		# handle interruption from keyboard
		print
		pass
	except:
		# handle all other exceptions
		log(traceback.format_exc(), kind="EXCEPTION")
	finally:
		# stop accepting server
		accepter.stop_proxy()
